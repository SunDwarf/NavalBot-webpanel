import hashlib
import json

from flask import request
from flask.ext.login import login_required

from app import cfg_forum, cfg
from app import login
from bot.web import FlaskUsers
from bot.web.navalweb import status

from bot.Blueprints import management

@management.route("/api/cookie/reload", methods=["POST"])
@login_required
def reload():
    cfg_forum.reload()
    cfg.reload()
    return "", 200
    # TODO: Add more reload functions.
    # TODO: Add authentication.

@management.route("/api/cookie/status.json")
@login_required
def route_status():
    s = status()
    if not s[0]:
        ob = {"status": 0, "msg": s[1], "warnings": []}
    else:
        ob = {"status": 1, "msg": "<h4>Warning! Your bot may be misconfigured.</h4>", "warnings": s[1]}
    return json.dumps(ob), 200, {"Content-Type": "text/json"}

@management.route("/api/cookie/login", methods=["POST"])
def login_api():
    u = FlaskUsers.get_user(request.form['username'])
    if u is None:
        return json.dumps({"status": 1, "msg": "Unknown username."}), 200, {"Content-Type": "text/json"}
    h = hashlib.new("sha512")
    h.update(request.form['password'].encode() + request.form['username'].encode())
    if h.hexdigest() == u.password:
        login.login_user(u)
        return json.dumps({"status": 0, "msg": "Login successful."})
    else:
        return json.dumps({"status": 1, "msg": "Invalid password."}), 200, {"Content-Type": "text/json"}
