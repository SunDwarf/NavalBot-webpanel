from app import cfg_forum, app
from configmaster.YAMLConfigFile import YAMLConfigFile

from bot.libnavalbot import module_dict, web_modules, web_rules, rule_dict


def init_web():
    for module in module_dict:
        if hasattr(module_dict[module], 'init_web'):
            c = module_dict[module].init_web()
            if len(c) == 2:
                web_modules.append(c)
    app.jinja_env.globals.update(srv_modules=web_modules)
    for module in rule_dict:
        if hasattr(rule_dict[module], 'init_web'):
            c = rule_dict['module'].init_web()
            if len(c) == 2:
                web_rules.append(c)
    app.jinja_env.globals.update(srv_rules=web_rules)

def status():
    bad = False
    warnings = []
    # Go through our config elements.
    if not hasattr(cfg_forum.config, "apikey") or cfg_forum.config.apikey == "myapikey":
        warnings.append("Your API key has not been set. Make sure to change it in navalbot.yml.")
        bad = True
    if not hasattr(cfg_forum.config, "host") or cfg_forum.config.host == "myhost":
        warnings.append("Your host has not been set. Make sure to change it in navalbot.yml.")
        bad = True
    if not hasattr(cfg_forum.config, "username") or cfg_forum.config.username == "myusername":
        warnings.append("Your username has not been set. Make sure to change it in navalbot.yml.")
        bad = True
    # Check for potentially dangerous things in the config
    if not hasattr(cfg_forum.config, "blacklist"):
        warnings.append("You do not have a blacklist key in your config. Bad things will happen.")
        bad = True
    else:
        if not cfg_forum.config.blacklist:
            warnings.append("Your username blacklist is empty. Bad things will happen.")
        if not hasattr(cfg_forum.config, "username") or cfg_forum.config.username not in cfg_forum.config.blacklist:
            warnings.append("Your username blacklist does not contain the username of your bot. Bad things will happen.")
            bad = True
    if not hasattr(cfg_forum.config, "verify_requests"):
        warnings.append("Your bot does not have a verify_requests config key. Anyone can pretend to be your forum.")
        bad = True
    else:
        if not cfg_forum.config.verify_requests:
            warnings.append("Your bot does not verify requests. Anyone can pretend to be your forum.")
            bad = True
    if not hasattr(cfg_forum.config, "debug"):
        warnings.append("Your bot does not have a debug config key. The bot is running in debug mode by default.")
        bad = True
    else:
        if cfg_forum.config.debug:
            warnings.append("Your bot is running in debug mode. Bad things may happen.")
            bad = True
    # Construct the message.
    if bad:
        return True, warnings
    else:
        nmsg = "All looks good. Your bot is ready for production."
        return False, nmsg