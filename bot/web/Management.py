from flask import blueprints, render_template, redirect
from flask.ext.login import login_required, logout_user

from app import app
from bot import libnavalbot

from bot.Blueprints import management

def init():
    app.register_blueprint(management, url_prefix="/management")

def init_web():
    app.jinja_env.globals.update(modules=libnavalbot.module_dict,
                           rules=libnavalbot.rule_dict)
    return "management.index", "Management"


@management.route("/login/")
def login_render():
    return render_template("management/login.html")

@management.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect("/management/login", 302)

@management.route("/")
@login_required
def index():
    return render_template("management/index.html")
